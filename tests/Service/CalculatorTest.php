<?php

namespace Tests\Service;

use App\Service\Calculator;
use PHPUnit\Framework\TestCase;

class TestCalculator extends TestCase
{
    public function testAdd()
    {
        $sut = new Calculator();
        $this->assertEquals(2, $sut->add(1, 1));
    }

    public function testSubstract()
    {
        $sut = new Calculator();
        $this->assertEquals(0, $sut->substract(1, 1));
    }
}

