<?php

declare(strict_types=1);

namespace App\Service;

final class Calculator
{
    public function add($a, $b)
    {
        return $a + $b;
    }

    public function substract($a, $b)
    {
        return $a - $b;
    }
}
