Consignes du projet :

```
Pour votre projet vous devrez fork le repository https://gitlab.com/vmonjaret/ipssi_21-3.


Votre objectif est d’automatiser un maximum de chose et d’avoir la meilleure experience développeur.
Pour cela vous allez mettre en place:
* Un workflow Git (Git flow ou Github flow)
* Les templates de merge request et d’issues
* Docker, ceci est un projet utilisant Symfony vous aurez donc besoin de plusieurs service
	* PHP-FPM
	* Nginx
	* MySQL
* Un readme
* Un Makefile
* Une CI (gitlab-ci) qui devra automatiser au minimum:
* Le linting de vos Dockerfile
* Le linting du code source (avec la configuration de l’outil, pour rappel PHP-cs-fixer)
* Les tests unitaires
* Uniquement sur master, la création de vos images embarquant l’intégralité du code source et leur publication sur le registry de gitlab (l’objectif étant de pouvoir déployer les images en production sans avoir à faire de partage de volumes)

Pour rappel j’attends de vous un historique GIT propre et je vous encourage à être "RP" dans ce projet, dites vous que c’est un projet qui est open source où les taches pourrait être faite par d’autres personnes.
Projet à faire seul mais vous avez la possibilité de faire de la revue de code entre vous.

Le projet est à rendre pour le 26 Janvier à 23:59 tout commit fait après cette date sera ignoré
```


